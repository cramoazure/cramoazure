﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.WebJobs;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Cramo_CleanupMessages
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        //public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        //{
        //    log.WriteLine(message);
        //}


        public static void CleanupMessages()
        {

            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {
                SqlCommand _cmd = new SqlCommand("sp_CleanupMessages", _conn);

                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.CommandTimeout = 180;
                _conn.Open();
                try
                {
                    _cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }
                finally 
                { 
                    _conn.Close(); 
                }

            }


        }

    }
}
