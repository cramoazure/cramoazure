﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo_SearchAndRentAPI.Models;
using Cramo_SearchAndRentAPI.Filters;

namespace Cramo_SearchAndRentAPI.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class SettingsController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get()
        {

            String strResponse = "<h1>Settings:</h1><br/>";
            foreach (String k  in System.Configuration.ConfigurationManager.AppSettings.Keys)
            {
                strResponse += k + " <--> " + System.Configuration.ConfigurationManager.AppSettings[k] + "<br/>";
            }
            //strResponse += "cramo-esb-db <--> " +  Environment.GetEnvironmentVariable("cramo-esb-db");
            strResponse += "SQLAZURECONNSTR_cramo-esb-db <--> " + Environment.GetEnvironmentVariable("SQLAZURECONNSTR_cramo-esb-db");
            //strResponse += "SQLCONNSTR_cramo-esb-db <--> " + Environment.GetEnvironmentVariable("SQLCONNSTR_cramo-esb-db");

            strResponse += "<br/><br/>ProjectConstant: " + (new ProjectConstants()).getDBConnString();

            // HTTP response 200/400
            HttpResponseMessage response = new HttpResponseMessage();
            //response.Headers.Add("Content-Type", "text/html");
            response.Content = new StringContent(strResponse);
            response.StatusCode = HttpStatusCode.OK;

            return response;
        }

    }
}