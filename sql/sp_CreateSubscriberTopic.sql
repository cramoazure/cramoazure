﻿USE [cramoesb]
GO

/****** Object: SqlProcedure [dbo].[sp_CreateSubscriberTopic] Script Date: 2015-11-23 12:15:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_CreateSubscriberTopic]
	@SubscriberId int, 
	@TopicName nvarchar(50),
	@Status nvarchar(3) output, 
	@ResponseBody nvarchar(max) output
AS

SET NOCOUNT ON;

DECLARE @NrOfSubscribers INT;
DECLARE @NrOfTopics INT;
DECLARE @SubscriberName nvarchar(50);
DECLARE @TopicId INT;
DECLARE @tmpTopicName nvarchar(50);

-- Set HTTP status
SET @Status = 200

-- Validate subscriber and topicname
SET @NrOfSubscribers = (SELECT count(SubscriberID) FROM Subscriber where SubscriberID = @SubscriberId);
SET @NrOfTopics = (SELECT count(TopicId) FROM dbo.Topic where Name = @TopicName);

PRINT @NrOfSubscribers;
PRINT @NrOfTopics;

-- Wrong subscriber
IF @NrOfSubscribers = 0 AND @Status = 200 BEGIN
	SET @Status = 400;
	SET @ResponseBody = '<apiResponseMessage><message>Missing subscriber: ' + CAST(@SubscriberId AS nvarchar(50)) + '</message><type>error</type></apiResponseMessage>';
END
-- Unknown topic
IF @NrOfTopics = 0 AND @Status = 200 BEGIN
	SET @Status = 400;
	SET @ResponseBody = '<apiResponseMessage><message>Unknown topic: ' + @TopicName + '</message><type>error</type></apiResponseMessage>';
END

--PRINT '[' + @TopicName + ']'
--PRINT '[' + CAST(@SubscriberId as NVARCHAR(5))+ ']'
--PRINT '[' + CAST(@NrOfSubscribers  as nvarchar(5))+ ']'

-- No topic sent, send list of topics you subscribe to
IF @NrOfSubscribers = 1 AND @TopicName = '' BEGIN
	SET @Status = 400;

	DECLARE @XMLStartNT nvarchar(500);
	DECLARE @XMLTopicsNT nvarchar(max);
	DECLARE @XMLEndNT nvarchar(150);

	DECLARE @getidNT CURSOR

	PRINT '[No topic given]'
	SELECT @SubscriberName = [Name] FROM Subscriber where SubscriberID = @SubscriberId;
	IF @SubscriberName IS NULL BEGIN
		SET @SubscriberName = '';
	END
	PRINT '[' + @SubscriberName + ']'

	SET @XMLTopicsNT = '';
	SET @XMLStartNT = '<register_subscriber_topic><subscriberId>' + CAST(@SubscriberId AS nvarchar(50)) +'</subscriberId><subscriberName>' + @SubscriberName + '</subscriberName><status>Ok, but no topic passed. Listing your current topics</status><topics>';
	SET @XMLEndNT = '</topics></register_subscriber_topic>';

	-- Get the active topics
	SET @getidNT = CURSOR FOR
		SELECT	SubscriberTopic.TopicID, Topic.Name
		FROM	SubscriberTopic, Topic
		WHERE	Topic.TopicID = SubscriberTopic.TopicID and  SubscriberTopic.SubscriberID = @SubscriberId 
		ORDER BY SubscriberTopic.TopicID;

	OPEN @getidNT
	FETCH NEXT FROM @getidNT INTO @TopicId, @tmpTopicName

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @XMLTopicsNT = @XMLTopicsNT + '<topic><id>' + CAST(@TopicId AS nvarchar(50)) +  '</id><name>' + @tmpTopicName + '</name></topic>';
		FETCH NEXT FROM @getidNT INTO @TopicId, @tmpTopicName
	END

	CLOSE @getidNT
	DEALLOCATE @getidNT

	SET @ResponseBody = @XMLStartNT + @XMLTopicsNT + @XMLEndNT;

END


-- Register topic
IF @Status = 200 BEGIN

	DECLARE @XMLStart nvarchar(250);
	DECLARE @XMLTopics nvarchar(max);
	DECLARE @XMLEnd nvarchar(150);

	DECLARE @getid CURSOR

	SELECT @TopicId = TopicId FROM dbo.Topic where Name = @TopicName;
	SELECT @SubscriberName = [Name] FROM Subscriber where SubscriberID = @SubscriberId;

	-- Insert relation
	BEGIN TRY
		INSERT INTO SubscriberTopic (SubscriberID, TopicID) VALUES (@SubscriberId, @TopicId);
	END TRY
	BEGIN CATCH 
		-- Possible future handling, Status=400?
	END CATCH

	SET @XMLTopics = '';
	SET @XMLStart = '<register_subscriber_topic><subscriberId>' + CAST(@SubscriberId AS nvarchar(50)) +'</subscriberId><subscriberName>' + @SubscriberName + '</subscriberName><status>Ok</status><topics>';
	SET @XMLEnd = '</topics></register_subscriber_topic>';

	-- Get the active topics
	SET @getid = CURSOR FOR
		SELECT	SubscriberTopic.TopicID, Topic.Name
		FROM	SubscriberTopic, Topic
		WHERE	Topic.TopicID = SubscriberTopic.TopicID and  SubscriberTopic.SubscriberID = @SubscriberId 
		ORDER BY SubscriberTopic.TopicID;

	OPEN @getid
	FETCH NEXT FROM @getid INTO @TopicId, @tmpTopicName

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @XMLTopics = @XMLTopics + '<topic><id>' + CAST(@TopicId AS nvarchar(50)) +  '</id><name>' + @tmpTopicName + '</name></topic>';
		FETCH NEXT FROM @getid INTO @TopicId, @tmpTopicName
	END

	CLOSE @getid
	DEALLOCATE @getid

	SET @ResponseBody = @XMLStart + @XMLTopics + @XMLEnd;
END
