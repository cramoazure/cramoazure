﻿USE [cramoesb]
GO

/****** Object: SqlProcedure [dbo].[sp_PostNewItem] Script Date: 2015-11-23 14:18:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_PostNewOrder]
	@MessageBody nvarchar(max),
	@Url nvarchar(255) OUTPUT,
	@Status nvarchar(3) OUTPUT, 
	@ResponseBody nvarchar(max) OUTPUT 
AS


	SET NOCOUNT ON;

	DECLARE @now datetime2;
	DECLARE @tomorrow datetime2;
	DECLARE @nextWorkday datetime2;
	DECLARE @currentTimeStamp nvarchar(25);
	DECLARE @tomorrowTimeStamp nvarchar(25);
	DECLARE @workdayTimeStamp nvarchar(25);
	DECLARE @hours int;
	DECLARE @weekday int;
	DECLARE @timeOfDay nvarchar(2);
	DECLARE @xmlProperty nvarchar(400);

	DECLARE @localKey nvarchar(50);
	DECLARE @localVariant nvarchar(50);
	DECLARE @localResult nvarchar(150);

	-- Init
	SET @xmlProperty = '';
	SET DATEFIRST 1; -- Monday is the first day of the week
	SET @Status = '200';
	SET @localKey = 'Systems';
	SET @localVariant = '360URI';

	-- Get server base URL
	EXEC [dbo].[sp_GetConfiguration]
		@Key = @localKey,
		@Variant = @localVariant,
		@ConfigValue = @localResult OUTPUT

	SET @Url = @localResult + '/order/create';

	--PRINT @Url

	SELECT @now = switchoffset (CONVERT(datetimeoffset, GETDATE()), '+01:00'); 
	PRINT @now;
	SELECT @tomorrow = DATEADD (dd , 1 , @now);
	PRINT @tomorrow;

	-- The actual timestamp
	SELECT @currentTimeStamp = convert(varchar,@now, 126);
	PRINT @currentTimeStamp;
	SET @currentTimeStamp = SUBSTRING(@currentTimeStamp, 1, 19) + '+0100';

	-- The end of tomorrow
	SELECT @tomorrowTimeStamp = convert(varchar,@tomorrow, 126);
	PRINT @tomorrowTimeStamp;
	SET @tomorrowTimeStamp = SUBSTRING(@tomorrowTimeStamp, 1, 11) + '16:00:00+0100';

	-- Next workday
	SET @nextWorkday = @tomorrow;
	SELECT @weekday = DATEPART(dw, @nextWorkDay);

	IF @weekday = 6 BEGIN -- Saturday. Add two days
		SELECT @nextWorkday = DATEADD (dd , 2 , @nextWorkday);
	END

	IF @weekday = 7 BEGIN -- Sunday. Add one day
		SELECT @nextWorkday = DATEADD (dd , 1 , @nextWorkday);
	END

	SELECT @workdayTimeStamp = convert(varchar,@nextWorkday, 126);
	PRINT @workdayTimeStamp;
	SET @workdayTimeStamp = SUBSTRING(@workdayTimeStamp, 1, 11) + '12:00:00+0100';

	-- Add to XML output
	-- New XML including database info
	SET @xmlProperty = @xmlProperty + '<Property key="DateToday">' + @currentTimeStamp + '</Property>';
	SET @xmlProperty = @xmlProperty + '<Property key="DateTomorrow">' + @tomorrowTimeStamp + '</Property>';
	SET @xmlProperty = @xmlProperty + '<Property key="DateNextWorkday">' + @workdayTimeStamp + '</Property>';

	--PRINT @currentTimeStamp;
	--PRINT @tomorrowTimeStamp;
	--PRINT @workdayTimeStamp;

	-- Get Hours. < 12 -> AM else PM
	SET @hours = DATEPART(hh ,@now);
	--PRINT @hours;

	IF @hours < 12 BEGIN
			SET @timeOfDay = 'AM';
		END
	ELSE
		BEGIN
			SET @timeOfDay = 'PM';
		END

	SET @xmlProperty = @xmlProperty + '<Property key="DSTPeriod">' + @timeOfDay + '</Property>';

	-- Get translations
	DECLARE @tmpKey        NVARCHAR(80);
    DECLARE @tmpFromValue  NVARCHAR(150);
    DECLARE @tmpToValue    NVARCHAR(150);
	DECLARE @tmpTranslations nvarchar(max);
	DECLARE @getid CURSOR

	SET @tmpTranslations = '';
	SET @getid = CURSOR FOR
		SELECT [Key], [FromValue], [ToValue] 
		FROM [dbo].[Translations] 
		WHERE [Key] like 'eCom.%.RentalDB';

	OPEN @getid
	FETCH NEXT FROM @getid INTO @tmpKey, @tmpFromValue, @tmpToValue

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @tmpTranslations = @tmpTranslations + '<Translation key="' + @tmpKey +  '"><from>' + @tmpFromValue +  '</from><to>' + @tmpToValue + '</to></Translation>';
		FETCH NEXT FROM @getid INTO @tmpKey, @tmpFromValue, @tmpToValue
	END


	SET @MessageBody = REPLACE(@MessageBody,'</order>', @xmlProperty + @tmpTranslations + '</order>');
	SET @ResponseBody = @MessageBody;

	--PRINT @timeOfDay;
	--PRINT @xmlProperty;

