﻿USE [cramoesb]
GO

/****** Object: SqlProcedure [dbo].[sp_GetConfiguration] Script Date: 2015-11-24 11:19:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_GetTranslation]
@TranslationKey nvarchar(80),
@FromValue nvarchar(150),
@ToValue nvarchar(150) output
AS

	SET NOCOUNT ON;

	DECLARE @tmpResult as nvarchar(150);

	SELECT @tmpResult = [ToValue] FROM dbo.Translations WHERE [Key] = @TranslationKey AND [FromValue] = @FromValue;

	PRINT '[' + @tmpResult + ']';

	IF @tmpResult IS NULL OR @tmpResult = '' BEGIN
			-- No match, return input
			SET @ToValue = @FromValue
		END
	ELSE
		BEGIN
			SET @ToValue = @tmpResult;
		END
