﻿USE [cramoesb]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- DROP TABLE [dbo].[Translations];

CREATE TABLE [dbo].[Translations] (
    [TranslationId] INT            IDENTITY (1, 1) NOT NULL,
    [Key]      NVARCHAR (80)  NOT NULL,
    [FromValue]  NVARCHAR (150)  NULL,
    [ToValue]    NVARCHAR (150) NULL
);

-- INSERT VALUES FOR BASWARE COMPANIES
--Test     Prod
--FJ <<-->> FP
--MJ <<-->> MP
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'Rental.Basware.Company.Outbound', N'FJ', N'FP');
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'Rental.Basware.Company.Outbound', N'MJ', N'MP');
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'Rental.Basware.Company.Outbound', N'SJ', N'SP');

-- SELECT * FROM [dbo].[Translations];

-- ORDER MARKET TRANSLATIONS
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'eCom.Market.RentalDB', N'7', N'SQ');
-- SE -> SQ
-- FI -> FQ
-- DK -> DQ
-- DE -> GQ
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'eCom.Market.RentalDB', N'SE', N'SQ');
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'eCom.Market.RentalDB', N'FI', N'FQ');
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'eCom.Market.RentalDB', N'DK', N'DQ');
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'eCom.Market.RentalDB', N'DE', N'GQ');
-- ORDER PAYMENT TYPES
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'eCom.Payment.RentalDB', N'Invoice', N'XR');
INSERT INTO [dbo].[Translations] ( [Key], [FromValue], [ToValue]) VALUES ( N'eCom.Payment.RentalDB', N'Cash', N'XC');

-- SELECT [Key], [FromValue], [ToValue] FROM [dbo].[Translations] WHERE [Key] like 'eCom.%.RentalDB';


