USE [cramoesb]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- DROP TABLE [dbo].[Log];

CREATE TABLE [dbo].[Log] (
    [LogId]	   INT IDENTITY (1, 1) NOT NULL,
	[TimeStamp] [datetime] NULL CONSTRAINT DF_Log_LogCreated DEFAULT (getdate()),
	[Application] VARCHAR(80) NULL,
    [Key]      VARCHAR (80)  NOT NULL,
    [LogStep]  VARCHAR (25) NOT NULL CONSTRAINT DF_Log_DefaultValue DEFAULT '', 
    [LogText]  VARCHAR (500) NULL,
	[Message]  varchar (max) NULL
);