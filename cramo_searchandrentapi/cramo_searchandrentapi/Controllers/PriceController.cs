﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo_SearchAndRentAPI.resources.utils;
using Cramo_SearchAndRentAPI.Filters;


namespace Cramo_SearchAndRentAPI.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class PriceController : ApiController
    {
        // POST 
        public HttpResponseMessage Get(String marketId,
            String productId,
            String customerId,
            String depotId,
            String fromDate,
            String toDate = null,
            String timeUnit = null,
            String quantity = "1")

        {

            String httpSuffix = ProjectConstants.priceGetTimeUnitPriceSuffix;
            String xmlContent = "";
            String requestId = Guid.NewGuid().ToString();

            XMLHelper xmlObj = new XMLHelper();
            DBHelper dbObj = new DBHelper();
            HTTPHelper httpObj = new HTTPHelper();

            // Init database
            dbObj.init();

            // SETTINGS ---------------------------------------------------
            // Systems 360URI
            String tmpUri = "https://cramoapi.azure-api.net/360/price/timeunitlist"; // Default
            String strServiceTimeUnit = "/price/timeunitlist";
            String strServicePrice = "/price/list";

            String uri = dbObj.getConfiguration("Systems", "360URI");
            //dbObj.log("PriceRequestTo360", "Price_" + requestId, "01", "Fetched URI -> ", uri);
            if (uri == null || uri.Trim().Equals("")) { uri = tmpUri; } 
            
            // Systems APIKey.1
            String tmpApiKey = "1d6535702d9d4dfbb876ec609233d83b"; // Default
            String apiKey = dbObj.getConfiguration("Systems", "APIKey.1");
            //dbObj.log("PriceRequestTo360", "Price_" + requestId, "02", "Fetched APIKey -> ", apiKey);
            if (apiKey == null || apiKey.Trim().Equals("")) { apiKey = tmpApiKey; }

            // translate marketid
            //marketId = dbObj.translateMarket(marketId);
            //dbObj.log("PriceRequestTo360", "Price_" + requestId, "05", "Market translated to -> " + marketId, "");

            // Check kind of price, GetPrice or TimeUnitPrice
            if (timeUnit != null && !timeUnit.Equals("") && toDate != null && !toDate.Equals(""))
            {
                // We have to request order price, unit selected 
                httpSuffix = ProjectConstants.priceGetPriceSuffix;
                // Create XML ------------------------
                xmlContent = xmlObj.getPriceXML(marketId, productId, customerId, depotId, fromDate, quantity, toDate, timeUnit);
                // Set service
                uri = uri + strServicePrice;

                // Log price start
                dbObj.log("PriceRequestTo360", "Price_" + requestId, "10", "Price request initiated from " + uri, xmlContent);
            }
            else
            {
                // Item price request, several units
                // Create XML ------------------------
                xmlContent = xmlObj.getTimeUnitXML(marketId, productId, customerId, depotId, fromDate, quantity);
                // Set service
                uri = uri + strServiceTimeUnit;

                // Log price start
                dbObj.log("PriceRequestTo360", "Price_" + requestId, "10", "TimeUnit request initiated from " + uri, xmlContent);
            }

            // HTTP POST
            String xmlResponse = httpObj.createHTTPPost(xmlContent, uri, apiKey);
            //dbObj.log("PriceRequestTo360", "Price_" + requestId, "15", "Price request sent to 360.", xmlResponse);

            // Transform response
            String xsltFile = ProjectConstants.getFileRootPath() + "\\resources\\xslt\\TransformPriceResponseToGenericResponse.xslt";
            String strResponse = xmlObj.transformXSLT(xmlResponse, xsltFile);

            // Log price request done
            dbObj.log("PriceRequestTo360", "Price_" + requestId, "20", "Price request done!", strResponse);

            // ClearUp
            dbObj.dispose();
            dbObj  = null;
            xmlObj = null;
            httpObj = null;

            // HTTP response 200/400
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent(strResponse);
            response.StatusCode = HttpStatusCode.OK;

            return response; // Request.CreateResponse(HttpStatusCode.OK, sw.ToString(), "text/xml");


        }
    }
}
