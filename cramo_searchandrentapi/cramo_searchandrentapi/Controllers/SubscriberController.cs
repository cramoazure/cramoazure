﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using Cramo.Utils.Authentication.Filters;

namespace Cramo_SearchAndRentAPI.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class SubscriberController : ApiController
    {
        // Creates a new subscriber with the specified name
        [Route("api/v1/Subscriber/{name}")]
        [HttpPost]
        public HttpResponseMessage Post(string name)
        {

            var connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string connectionString = connectionStringEntry.ConnectionString;

            HttpResponseMessage response = new HttpResponseMessage();
            
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("sp_CreateSubscriber", conn);

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter responseParam = new SqlParameter();
                responseParam.ParameterName = "@ResponseBody";
                responseParam.Direction = ParameterDirection.Output;
                responseParam.DbType = DbType.Xml;
                responseParam.Size = 1000;

                command.Parameters.Add(new SqlParameter("@Name", name));
                command.Parameters.Add(responseParam);

                conn.Open();
                command.ExecuteNonQuery();
               
                response.Content = new StringContent(command.Parameters["@ResponseBody"].Value.ToString());
                response.StatusCode = HttpStatusCode.OK;

                conn.Close();
            }

            return response;

            }
        
    }
}
