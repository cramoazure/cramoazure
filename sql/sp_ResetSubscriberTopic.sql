USE [cramoesb]
GO
/****** Object:  StoredProcedure [dbo].[sp_ResetSubscriberTopic]    Script Date: 2015-12-16 14:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_ResetSubscriberTopic]
@SubscriberId int,
@Topic varchar(30),
@Status int OUTPUT, 
@ResponseBody XML OUTPUT

AS

	SET NOCOUNT ON;

	DECLARE @topicID INT;
	
	SET @topicID = (SELECT topicID FROM dbo.Topic WHERE Name = @Topic);

	IF @TopicID IS NULL BEGIN
		-- ERROR, NOT A VALID TOPIC
		SET @ResponseBody = '<apiResponseMessage><message>Unknown topic (' + @Topic + ')</message><type>error</type></apiResponseMessage>'
		SET @Status = 400;
	END
	ELSE 
	BEGIN
	
	BEGIN TRANSACTION

		UPDATE q 
		SET Delivered = CONVERT(DATETIME, '1970-01-01')
		FROM dbo.SubscriberMessageQueue q
		JOIN dbo.Message m ON q.messageId = m.messageId
		WHERE 
			subscriberid = @subscriberId AND 
			topicid = @topicid AND 
			Delivered IS NULL
		
	COMMIT TRANSACTION

	SET @Status = 200;
	SET @ResponseBody = '<apiResponseMessage><message>All messages in topic ' + @Topic + ' for subscriber ' + CONVERT(nvarchar(3), @SubscriberId) + ' has been marked as delivered.</message><type>ok</type></apiResponseMessage>';

	END

	

