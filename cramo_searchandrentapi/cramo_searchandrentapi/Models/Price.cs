﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cramo_SearchAndRentAPI.Models
{
    public class Price
    {
        public String productId { get; set; }
        public String customerId { get; set; }
        public String depotId { get; set; }
        public String fromDate { get; set; }
        public String toDate { get; set; }
        public String timeUnit { get; set; }
        public String quantity { get; set; }
    }
}