﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Xsl;

namespace Cramo_SearchAndRentAPI.resources.utils
{
    public class XMLHelper
    {

        // Create PriceRequest
        public String getTimeUnitXML(String marketId, String productId, String customerId, String depotId, String fromDate, String qty)
        {
            GenericHelper helper = new GenericHelper();
            String returnString = "";

            returnString = returnString + "<cramo:GetTimeUnitPrices xmlns:cramo=\"http://www.cramo.com/canonical\">";
            returnString = returnString + "  <cramo:ApplicationArea>";
            returnString = returnString + "    <cramo:Sender>";
            returnString = returnString + "      <cramo:ComponentID>EPiServer</cramo:ComponentID>";
            returnString = returnString + "      <cramo:TaskID>Price request</cramo:TaskID>";
            returnString = returnString + "      <cramo:ReferenceID>" + marketId + "</cramo:ReferenceID>"; 
            returnString = returnString + "    </cramo:Sender>";
            returnString = returnString + "    <cramo:CreationDateTime>" + helper.getCurrentTimeStamp() + "</cramo:CreationDateTime>"; 
            returnString = returnString + "  </cramo:ApplicationArea>";
            returnString = returnString + "  <cramo:DataArea>";
            returnString = returnString + "    <cramo:TimeUnitPriceRequest>";
            returnString = returnString + "      <!-- Fixed ordertype from Web/eCom-->";
            returnString = returnString + "      <cramo:OrderType>XR</cramo:OrderType>";
            returnString = returnString + "      <cramo:Item>" + productId + "</cramo:Item>"; //Item
            returnString = returnString + "      <cramo:Quantity>" + qty + "</cramo:Quantity>"; // Qty
            returnString = returnString + "      <cramo:Customer>" + customerId + "</cramo:Customer>"; // Customer number
            returnString = returnString + "      <cramo:OrganizationalUnit>" + depotId + "</cramo:OrganizationalUnit>"; // Depot
            returnString = returnString + "      <cramo:PriceFromDate>" + fromDate + "</cramo:PriceFromDate>";
            returnString = returnString + "    </cramo:TimeUnitPriceRequest>";
            returnString = returnString + "  </cramo:DataArea>";
            returnString = returnString + "</cramo:GetTimeUnitPrices>";

            return returnString;
        }


        public String transformXSLT(String strXmlInput, String strXSLTFilePath) {

            String transformResult = "";

            // Version 2
            XslCompiledTransform xslt = new XslCompiledTransform();
            xslt.Load(strXSLTFilePath);

            StringBuilder sb = new StringBuilder();
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.Encoding = Encoding.UTF8;
            writerSettings.Indent = true;
            writerSettings.OmitXmlDeclaration = true;

            xslt.Transform(XmlReader.Create(new StringReader(strXmlInput)), XmlWriter.Create(sb, writerSettings));
            transformResult = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + sb.ToString();

            return transformResult;
        }

        public String getPriceXML(string marketId, string productId, string customerId, string depotId, string fromDate, string qty, string toDate, string timeUnit)
        {
            GenericHelper helper = new GenericHelper();
            String returnString = "";

            returnString = returnString + "<cramo:GetPrices xmlns:cramo=\"http://www.cramo.com/canonical\">";
            returnString = returnString + "  <cramo:ApplicationArea>";
            returnString = returnString + "    <cramo:Sender>";
            returnString = returnString + "      <cramo:ComponentID>EPiServer</cramo:ComponentID>";
            returnString = returnString + "      <cramo:TaskID>Price request</cramo:TaskID>";
            returnString = returnString + "      <cramo:ReferenceID>" + marketId + "</cramo:ReferenceID>";
            returnString = returnString + "    </cramo:Sender>";
            returnString = returnString + "    <cramo:CreationDateTime>" + helper.getCurrentTimeStamp() + "</cramo:CreationDateTime>";
            returnString = returnString + "  </cramo:ApplicationArea>";
            returnString = returnString + "  <cramo:DataArea>";
            returnString = returnString + "    <cramo:PriceRequest>";
            returnString = returnString + "      <!-- Fixed ordertype from Web/eCom-->";
            returnString = returnString + "      <cramo:OrderType>XR</cramo:OrderType>";
            returnString = returnString + "      <cramo:Item>" + productId + "</cramo:Item>"; //Item
            returnString = returnString + "      <cramo:Quantity>" + qty + "</cramo:Quantity>"; // Qty
            returnString = returnString + "      <cramo:Customer>" + customerId + "</cramo:Customer>"; // Customer number
            returnString = returnString + "      <cramo:OrganizationalUnit>" + depotId + "</cramo:OrganizationalUnit>"; // Depot
            returnString = returnString + "      <cramo:PriceFromDate>" + fromDate + "</cramo:PriceFromDate>";
            returnString = returnString + "      <cramo:PriceToDate>" + toDate + "</cramo:PriceToDate>";
            returnString = returnString + "      <cramo:TimeUnit>" + timeUnit + "</cramo:TimeUnit>"; 
            returnString = returnString + "    </cramo:PriceRequest>";
            returnString = returnString + "  </cramo:DataArea>";
            returnString = returnString + "</cramo:GetPrices>";

            return returnString;
        }
    }
}