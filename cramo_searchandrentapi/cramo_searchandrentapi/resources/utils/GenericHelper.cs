﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cramo_SearchAndRentAPI.resources.utils
{
    public class GenericHelper
    {
        public String getCurrentTimeStamp()
        {
            DateTime timeStamp = DateTime.UtcNow;
            timeStamp = timeStamp.AddHours(1.0);
            return timeStamp.ToString("yyyy-MM-ddTHH:mm:ss");
        }
    }
}