﻿
/*
CREATE TABLE [dbo].[Configuration]
(
	[ConfigId] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [Key] NVARCHAR(50) NOT NULL, 
    [Variant] NVARCHAR(50) NULL, 
    [Value] NVARCHAR(150) NULL
)

*/

-- INSERT INTO dbo.Configuration ([Key], [Variant], [Value]) VALUES ('Documents', 'SRAURI', 'http://srakod.se/product/resources?customer=cramo&locale=sv&connection='); 

select * from dbo.Configuration;

INSERT INTO dbo.Configuration ([Key], [Variant], [Value]) VALUES ('Systems', 'InRiverURI', 'http://sebospimt01:9090'); 
INSERT INTO dbo.Configuration ([Key], [Variant], [Value]) VALUES ('Systems', '360URI', 'http://crainttest.cramo.internal:8443/'); 
INSERT INTO dbo.Configuration ([Key], [Variant], [Value]) VALUES ('Systems', 'APIKey.1', '1d6535702d9d4dfbb876ec609233d83b'); 

-- Rental databases
INSERT INTO dbo.Configuration ([Key], [Variant], [Value]) VALUES ('RentalDB', 'ItemMaster', 'CE'); 
