﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Cramo_SearchAndRentAPI.Models
{
    public static class OrderValidator
    {

        private static XNamespace _oagis = "http://www.openapplications.org/oagis/10";

        public static bool IsOrderValid(string orderBody)
        {
            XElement order = XElement.Load(new StringReader(orderBody));

            XElement market = order.Element(_oagis + "ApplicationArea").Element(_oagis + "Sender").Element(_oagis + "ReferenceID");
            XElement customer = order.Element(_oagis + "DataArea").Element(_oagis + "SalesOrder").Element(_oagis + "SalesOrderHeader").Element(_oagis + "BillToParty").Element(_oagis + "ID");

            string searchString = market.Value.ToUpper() + ":" + customer.Value;

            string activeCustomers = ConfigurationManager.AppSettings["ActiveCustomers"];

            return activeCustomers.Contains(searchString);

        }

    }
}