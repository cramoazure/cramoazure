﻿using Cramo_SearchAndRentAPI.Models;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;


namespace Cramo_SearchAndRentAPI.resources.utils
{
    public class DBHelper
    {

        public void init() { }

        public void dispose() { }

        public void log(String applicationName, String requestKey, String stepId, String logText, String logMessage = "") {

            // Call SP and translation
            // TODO: Move to init and re-use
            String connStr = (new ProjectConstants()).getDBConnString();

            using (SqlConnection conn = new SqlConnection(connStr))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("sp_InsertLogRecord", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@LogApplication", applicationName));
                cmd.Parameters.Add(new SqlParameter("@LogKey", requestKey));
                cmd.Parameters.Add(new SqlParameter("@LogStep", stepId));
                cmd.Parameters.Add(new SqlParameter("@LogText", logText));
                cmd.Parameters.Add(new SqlParameter("@LogMessage", logMessage));

                // execute the command
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public String translateMarket(String marketId)
        {
            String returnValue = "";
            String _TRANSLATIONKEY = "eCom.Market.RentalDB";

            if (marketId == null) { returnValue = ""; } else { returnValue = marketId.Trim().ToUpper(); }

            // Call SP and translation
            // TODO: Move to init and re-use
            String connStr = (new ProjectConstants()).getDBConnString();

            using (SqlConnection conn = new SqlConnection(connStr))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("sp_GetTranslation", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@TranslationKey", _TRANSLATIONKEY));
                cmd.Parameters.Add(new SqlParameter("@FromValue", marketId));

                SqlParameter pResponse = new SqlParameter();
                pResponse.ParameterName = "@ToValue";
                pResponse.DbType = DbType.String;
                pResponse.Size = 150;
                pResponse.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pResponse);

                // execute the command
                conn.Open();
                cmd.ExecuteNonQuery();

                returnValue = cmd.Parameters["@ToValue"].Value.ToString();
                conn.Close();
            }
            
            return returnValue;
        }

        public String getConfiguration(String key, String variant)
        {
            String returnValue = "";

            if (key == null || key.Trim().Equals("")) { return null; }
            if (variant == null || variant.Trim().Equals("")) { return null; }

            // Call SP and translation
            // TODO: Move to init and re-use
            String connStr = (new ProjectConstants()).getDBConnString();

            using (SqlConnection conn = new SqlConnection(connStr))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("sp_GetConfiguration", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@Key", key));
                cmd.Parameters.Add(new SqlParameter("@Variant", variant));

                SqlParameter pResponse = new SqlParameter();
                pResponse.ParameterName = "@ConfigValue";
                pResponse.DbType = DbType.String;
                pResponse.Size = 150;
                pResponse.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pResponse);

                // execute the command
                conn.Open();
                cmd.ExecuteNonQuery();

                returnValue = cmd.Parameters["@ConfigValue"].Value.ToString();
                conn.Close();
            }

            return returnValue;
        }

        public ArrayList storeTopicMessage(TopicMessage message)
        {
            ArrayList returnValue = new ArrayList();

            // Get db-connection
            String connStr = (new ProjectConstants()).getDBConnString();

            using (SqlConnection conn = new SqlConnection(connStr))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("sp_PostMessage", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@MessageBody", message.messageBody));
                cmd.Parameters.Add(new SqlParameter("@Topic", message.topic));
                cmd.Parameters.Add(new SqlParameter("@SubscriberId", message.subscriberId));
                //cmd.Parameters.AddWithValue("@MessageBody", body);
                //cmd.Parameters.AddWithValue("@Topic", topic);

                SqlParameter pStatus = new SqlParameter();
                pStatus.ParameterName = "@Status";
                pStatus.DbType = DbType.Int16;
                pStatus.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pStatus);

                SqlParameter pResponse = new SqlParameter();
                pResponse.ParameterName = "@ResponseBody";
                pResponse.DbType = DbType.Xml;
                pResponse.Size = 1000;
                pResponse.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pResponse);

                // execute the command
                conn.Open();
                cmd.ExecuteNonQuery();

                returnValue.Add("" + cmd.Parameters["@Status"].Value.ToString());
                returnValue.Add(cmd.Parameters["@ResponseBody"].Value.ToString());
                conn.Close();

            }

            return returnValue;
        }

        public ArrayList resetSubscriberTopic(SubscriberTopic st)
        {
            ArrayList returnValue = new ArrayList();

            // Get db-connection
            String connStr = (new ProjectConstants()).getDBConnString();

            using (SqlConnection conn = new SqlConnection(connStr))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("sp_ResetSubscriberTopic", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@SubscriberId", st.subscriberId));
                cmd.Parameters.Add(new SqlParameter("@Topic", st.topicName));

                SqlParameter pStatus= new SqlParameter();
                pStatus.ParameterName = "@Status";
                pStatus.DbType = DbType.Int16;
                pStatus.Size = 1000;
                pStatus.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pStatus);
                                
                SqlParameter pResponse = new SqlParameter();
                pResponse.ParameterName = "@ResponseBody";
                pResponse.DbType = DbType.Xml;
                pResponse.Size = 1000;
                pResponse.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pResponse);

                // execute the command
                conn.Open();
                cmd.ExecuteNonQuery();

                returnValue.Add("" + cmd.Parameters["@Status"].Value.ToString());
                returnValue.Add(cmd.Parameters["@ResponseBody"].Value.ToString());
                conn.Close();

            }

            return returnValue;
        }

        public String getTopicMessages(String topic, String subscriberId, String batchSize)
        {

            String returnValue = null;
            int subId = int.Parse(subscriberId);

            // Get db-connection
            String connStr = (new ProjectConstants()).getDBConnString();

            using (SqlConnection conn = new SqlConnection(connStr))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("sp_ReadTopic", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@TopicName", topic));
                cmd.Parameters.Add(new SqlParameter("@SubscriberID", subId));

                SqlParameter pMsg = new SqlParameter();
                pMsg.ParameterName = "@body";
                pMsg.DbType = DbType.String;
                pMsg.Size = -1;
                pMsg.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pMsg);

                // execute the command
                conn.Open();
                cmd.ExecuteNonQuery();

                returnValue = cmd.Parameters["@body"].Value.ToString();
                conn.Close();

            }

            return returnValue;
        }
    }
}