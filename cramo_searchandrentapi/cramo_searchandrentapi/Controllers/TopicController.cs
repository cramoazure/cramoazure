﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo.Utils.Authentication.Filters;

namespace Cramo_SearchAndRentAPI.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class TopicController : ApiController
    {
        [Route("api/v1/Topic/{topicname}/Subscriptions/{id}")]
        [HttpPost]
        public HttpResponseMessage Post(string topicname, string id)
        {

            var connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string connectionString = connectionStringEntry.ConnectionString;

            HttpResponseMessage response = new HttpResponseMessage();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("sp_CreateSubscriberTopic", conn);

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter statusParam = new SqlParameter();
                statusParam.ParameterName = "@Status";
                statusParam.Direction = ParameterDirection.Output;
                statusParam.DbType = DbType.String;
                statusParam.Size = 3;

                SqlParameter responseParam = new SqlParameter();
                responseParam.ParameterName = "@ResponseBody";
                responseParam.Direction = ParameterDirection.Output;
                responseParam.DbType = DbType.String;
                responseParam.Size = 1000;
                
                command.Parameters.Add(new SqlParameter("@SubscriberId", id));
                command.Parameters.Add(new SqlParameter("@TopicName", topicname));
                command.Parameters.Add(statusParam);
                command.Parameters.Add(responseParam);

                conn.Open();
                command.ExecuteNonQuery();

                int status = Convert.ToInt32(command.Parameters["@Status"].Value);

                response.Content = new StringContent(command.Parameters["@ResponseBody"].Value.ToString());
                response.StatusCode = (HttpStatusCode)status;

                conn.Close();
            }

            return response;
        
        
        }

        [Route("api/v1/Topic/{topicname}/Subscriptions/{id}")]
        [HttpDelete]
        public HttpResponseMessage Delete(string topicname, string id)
        {
            var connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string connectionString = connectionStringEntry.ConnectionString;

            HttpResponseMessage response = new HttpResponseMessage();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("sp_DeleteSubscriberTopic", conn);

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter statusParam = new SqlParameter();
                statusParam.ParameterName = "@Status";
                statusParam.Direction = ParameterDirection.Output;
                statusParam.DbType = DbType.String;
                statusParam.Size = 3;

                SqlParameter responseParam = new SqlParameter();
                responseParam.ParameterName = "@ResponseBody";
                responseParam.Direction = ParameterDirection.Output;
                responseParam.DbType = DbType.String;
                responseParam.Size = 1000;

                command.Parameters.Add(new SqlParameter("@SubscriberId", id));
                command.Parameters.Add(new SqlParameter("@TopicName", topicname));
                command.Parameters.Add(statusParam);
                command.Parameters.Add(responseParam);

                conn.Open();
                command.ExecuteNonQuery();

                int status = Convert.ToInt32(command.Parameters["@Status"].Value);

                response.Content = new StringContent(command.Parameters["@ResponseBody"].Value.ToString());
                response.StatusCode = (HttpStatusCode)status;

                conn.Close();
            }

            return response;
        }
    }
}
