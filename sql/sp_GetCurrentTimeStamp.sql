﻿USE [cramoesb]
GO

/****** Object: SqlProcedure [dbo].[sp_PostNewItem] Script Date: 2015-11-23 14:18:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetCurrentTimeStamp]
	@currentTimeStamp varchar(25) OUTPUT 
AS


	SET NOCOUNT ON;

	DECLARE @now datetime2;

	-- Get current timestamp
	SELECT @now = switchoffset (CONVERT(datetimeoffset, GETDATE()), '+01:00'); 
	SELECT @currentTimeStamp = convert(varchar,@now, 126);
	SET @currentTimeStamp = SUBSTRING(@currentTimeStamp, 1, 19) + '+0100';
	PRINT @currentTimeStamp;