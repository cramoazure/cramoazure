﻿USE [cramoesb]
GO

/****** Object: SqlProcedure [dbo].[sp_CreateSubscriberTopic] Script Date: 2015-11-23 16:32:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_DeleteSubscriberTopic]
	@SubscriberId int, 
	@TopicName nvarchar(50),
	@Status nvarchar(3) output, 
	@ResponseBody nvarchar(max) output
AS

SET NOCOUNT ON;

DECLARE @NrOfSubscribers INT;
DECLARE @NrOfTopics INT;
DECLARE @SubscriberName nvarchar(50);
DECLARE @TopicId INT;
DECLARE @tmpTopicName nvarchar(50);

-- Set HTTP status
SET @Status = 200

-- Validate subscriber and topicname
SET @NrOfSubscribers = (SELECT count(SubscriberID) FROM Subscriber where SubscriberID = @SubscriberId);
SET @NrOfTopics = (SELECT count(TopicId) FROM dbo.Topic where Name = @TopicName);

PRINT @NrOfSubscribers;
PRINT @NrOfTopics;

-- Wrong subscriber
IF @NrOfSubscribers = 0 AND @Status = 200 BEGIN
	SET @Status = 400;
	SET @ResponseBody = '<apiResponseMessage><message>Missing subscriber: ' + CAST(@SubscriberId AS nvarchar(50)) + '</message><type>error</type></apiResponseMessage>';
END
-- Unknown topic or blank
IF @TopicName = '' AND @Status = 200 BEGIN
	SET @Status = 400;
	SET @ResponseBody = '<apiResponseMessage><message>Unknown topic: blank</message><type>error</type></apiResponseMessage>';
END
IF @NrOfTopics = 0 AND @Status = 200 BEGIN
	SET @Status = 400;
	SET @ResponseBody = '<apiResponseMessage><message>Unknown topic: ' + @TopicName + '</message><type>error</type></apiResponseMessage>';
END


-- Unregister topic
IF @Status = 200 BEGIN

	DECLARE @XMLStart nvarchar(250);
	DECLARE @XMLTopics nvarchar(max);
	DECLARE @XMLEnd nvarchar(150);

	DECLARE @getid CURSOR

	SELECT @TopicId = TopicId FROM dbo.Topic where Name = @TopicName;
	SELECT @SubscriberName = [Name] FROM Subscriber where SubscriberID = @SubscriberId;

	-- Delete relation
	BEGIN TRY
		DELETE FROM SubscriberTopic WHERE SubscriberID = @SubscriberId AND TopicID = @TopicId;
	END TRY
	BEGIN CATCH 
		-- Possible future handling, Status=400?
	END CATCH

	SET @XMLTopics = '';
	SET @XMLStart = '<unregister_subscriber_topic><subscriberId>' + CAST(@SubscriberId AS nvarchar(50)) +'</subscriberId><subscriberName>' + @SubscriberName + '</subscriberName><status>Ok</status><topics>';
	SET @XMLEnd = '</topics></unregister_subscriber_topic>';

	-- Get the active topics
	SET @getid = CURSOR FOR
		SELECT	SubscriberTopic.TopicID, Topic.Name
		FROM	SubscriberTopic, Topic
		WHERE	Topic.TopicID = SubscriberTopic.TopicID and  SubscriberTopic.SubscriberID = @SubscriberId 
		ORDER BY SubscriberTopic.TopicID;

	OPEN @getid
	FETCH NEXT FROM @getid INTO @TopicId, @tmpTopicName

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @XMLTopics = @XMLTopics + '<topic><id>' + CAST(@TopicId AS nvarchar(50)) +  '</id><name>' + @tmpTopicName + '</name></topic>';
		FETCH NEXT FROM @getid INTO @TopicId, @tmpTopicName
	END

	CLOSE @getid
	DEALLOCATE @getid

	SET @ResponseBody = @XMLStart + @XMLTopics + @XMLEnd;
END
