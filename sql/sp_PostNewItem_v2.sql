﻿USE [cramoesb]
GO

/****** Object: SqlProcedure [dbo].[sp_PostNewItem] Script Date: 2015-12-01 16:13:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_PostNewItem]
	@MessageBody nvarchar(max),
	@Url nvarchar(255) OUTPUT,
	@Status nvarchar(3) OUTPUT, 
	@ResponseBody nvarchar(max) OUTPUT
AS


	SET NOCOUNT ON;

	DECLARE @localKey nvarchar(50);
	DECLARE @localVariant nvarchar(50);
	DECLARE @localResult nvarchar(150);
	DECLARE @localRentalDB nvarchar(50);
	DECLARE @localRentalLang nvarchar(50);
	DECLARE @xmlProperty nvarchar(400);
	DECLARE	@GUID nvarchar(36); 

	SET @localKey = 'Systems';
	SET @localVariant = '360URI';
	SET @Status = '200';
	SET @localRentalDB = '';

	-- Get server base URL
	EXEC [dbo].[sp_GetConfiguration]
		@Key = @localKey,
		@Variant = @localVariant,
		@ConfigValue = @localResult OUTPUT

	SET @Url = @localResult + 'rental/item';

	PRINT @Url

	-- Get database property - database
	SET @localKey = 'RentalDB';
	SET @localVariant = 'ItemMaster';

	EXEC [dbo].[sp_GetConfiguration]
		@Key = @localKey,
		@Variant = @localVariant,
		@ConfigValue = @localResult OUTPUT

	SET @localRentalDB = @localResult;

	-- New XML including database info
	SET @xmlProperty = '<Property key="RentalDB">' + @localRentalDB + '</Property>';
	--PRINT @xmlProperty;

	-- Get database property - rental default language
	SET @localKey = 'RentalDB';
	SET @localVariant = 'ItemMasterLang';

	EXEC [dbo].[sp_GetConfiguration]
		@Key = @localKey,
		@Variant = @localVariant,
		@ConfigValue = @localResult OUTPUT

	SET @localRentalLang = @localResult;

	-- New GUID
	SET @GUID = cast(NEWID() as varchar(36));

	-- New XML including database info and language
	SET @xmlProperty = @xmlProperty + '<Property key="RentalDBLang">' + @localRentalLang + '</Property>';
	SET @xmlProperty = @xmlProperty + '<Property key="MsgGUID">' + @GUID + '</Property>';

	SET @MessageBody = REPLACE(@MessageBody,'</Item>', @xmlProperty + '</Item>');
	SET @ResponseBody = @MessageBody;
