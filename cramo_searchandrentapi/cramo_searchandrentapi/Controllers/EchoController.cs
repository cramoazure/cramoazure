﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo_SearchAndRentAPI.Models;
using Cramo_SearchAndRentAPI.Filters;

namespace Cramo_SearchAndRentAPI.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class EchoController : ApiController
    {
        [HttpGet]
        public IEnumerable<Echo> Get(String EchoString = "Echo")
        {
            return new Echo[] {
                new Echo { EchoString = "" + EchoString + " " + EchoString }
            };
        }
    }
}
