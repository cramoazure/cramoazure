CREATE PROCEDURE [dbo].[sp_GetFilename]
    @Datatype nvarchar(50),
    @RequestId nvarchar(50),
	@LogFileName nvarchar(255),
    @SFTPFileName nvarchar(100) output,
	@Status nvarchar(3) output, 
	@HTTPResponse nvarchar(max) output
AS
    SET NOCOUNT ON;
    SET @Status = '200';

	DECLARE @validationOfDatatype INT;
	-- Validate subscriber and topicname
	SET @validationOfDatatype = (SELECT count([Filename]) FROM [dbo].[BaswareDatatypes] WHERE Datatype = LOWER(@Datatype));
	PRINT @validationOfDatatype;

	IF @validationOfDatatype = 0 AND @Status = 200 BEGIN
		SET @Status = 400;
		SET @SFTPFileName = 'N/A';
		SET @HTTPResponse = '<apiResponseMessage><message>Missing datatype: ' + @Datatype + '. Unknown filename to SFTP!</message><type>error</type></apiResponseMessage>';
	END
	ELSE 
	BEGIN
		SELECT @SFTPFileName = [Filename] FROM [dbo].[BaswareDatatypes] WHERE Datatype = LOWER(@Datatype);
		IF CHARINDEX('#',@SFTPFileName) > 0
		BEGIN
			SET @SFTPFileName = REPLACE(@SFTPFileName,'#', @RequestId);
			SET @SFTPFileName = @SFTPFileName +'.xml';
		END
		SET @HTTPResponse = '<apiResponseMessage><message>File written to SFTP with filename: ' + @SFTPFileName + '</message><type>ok</type></apiResponseMessage>';
	END

	-- ToDo Log Filename
