﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cramo_SearchAndRentAPI
{

    public class ProjectConstants
    {

        public String dbConnectionString = "Server=tcp:cramointegrations.database.windows.net,1433;Database=cramoesb-Test;User ID=integrationusrtst;Password=AsW5ThcEyjP9ksf;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        public static String priceGetTimeUnitPriceSuffix = "/price/timeunitlist"; // Item price request
        public static String priceGetPriceSuffix = "/price/list"; // Order price
        public static String basicAuthUser = "intwebapiuser";
        public static String basicAuthPassword = "Y8xJlJ7imAJZz3u";

        public String getDBConnString()
        {
            // Define this as an Application setting / Connection String / Type="SQL Database" and Name="cramo-esb-db"
            String tmpDbConn = Environment.GetEnvironmentVariable("SQLAZURECONNSTR_cramo-esb-db");
            if (tmpDbConn != null && !tmpDbConn.Trim().Equals(""))
            {
                return tmpDbConn;
            } else
            {
                return this.dbConnectionString;
            }
            
        }

        public static String getFileRootPath()
        {
            return System.Web.HttpContext.Current.Server.MapPath("~");
        }
    }
}