﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cramo_SearchAndRentAPI.Models
{
    public class SubscriberTopic
    {
        public int subscriberId { get; set; }
        public String topicName { get; set; }
    }
}