﻿USE [cramoesb]
GO

/****** Object: SqlProcedure [dbo].[sp_BaswareGetFilename] Script Date: 2015-12-08 11:22:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_BaswareGetFilename]
    @Datatype nvarchar(50),
    @RequestId nvarchar(50),
	@LogFileName nvarchar(255),
	@MarketName nvarchar(10), 
    @SFTPFileName nvarchar(100) output,
	@Status nvarchar(3) output, 
	@HTTPResponse nvarchar(max) output
AS
    SET NOCOUNT ON;
    SET @Status = '200';

	DECLARE @validationOfDatatype INT;
	-- Validate subscriber and topicname
	SET @validationOfDatatype = (SELECT count([Filename]) FROM [dbo].[BaswareDatatypes] WHERE Datatype = LOWER(@Datatype));
	PRINT @validationOfDatatype;

	IF @validationOfDatatype = 0 AND @Status = 200 BEGIN
		SET @Status = '400';
		SET @SFTPFileName = 'N/A';
		SET @HTTPResponse = '<apiResponseMessage><message>Missing datatype: ' + @Datatype + '. No file written to SFTP</message><type>error</type></apiResponseMessage>';
	END
	ELSE 
	BEGIN
		-- Get timestamp
		DECLARE @now datetime2;
		DECLARE @timestamp nvarchar(30);

		SELECT @now = switchoffset (CONVERT(datetimeoffset, GETDATE()), '+01:00'); 
		SELECT @timestamp = format(@now,'yyyyMMddHHmmss')
		PRINT @timestamp;

		-- Get Market and translate if existing
		IF @MarketName IS NOT NULL AND @MarketName <> '' BEGIN
			DECLARE	@tmpValue nvarchar(150);
			DECLARE @tmpKey nvarchar(80);

			SET @tmpKey = N'Rental.Basware.Company.Outbound';

			EXEC [dbo].[sp_GetTranslation]
					@TranslationKey = @tmpKey,
					@FromValue = @MarketName,
					@ToValue = @tmpKey OUTPUT

			SET @MarketName = '_' + @tmpKey;
			--PRINT @MarketName;
		END

		SELECT @SFTPFileName = [Filename] FROM [dbo].[BaswareDatatypes] WHERE Datatype = LOWER(@Datatype);

		-- Replace Market
		IF CHARINDEX('$Market$',@SFTPFileName) > 0
		BEGIN
			SET @SFTPFileName = REPLACE(@SFTPFileName,'$Market$', @MarketName);
		END

		-- Replace Timestamp
		IF CHARINDEX('$Timestamp$',@SFTPFileName) > 0
		BEGIN
			SET @SFTPFileName = REPLACE(@SFTPFileName,'$Timestamp$', @timestamp);
		END

		-- Replace RequestId
		IF CHARINDEX('$RequestId$',@SFTPFileName) > 0
		BEGIN
			SET @SFTPFileName = REPLACE(@SFTPFileName,'$RequestId$', @RequestId);
		END

		--PRINT 'Filename: ' + @SFTPFileName;
		--SET @SFTPFileName = @SFTPFileName + '.xml';

		SET @HTTPResponse = '<apiResponseMessage><message>File written to SFTP with filename: ' + @SFTPFileName + '</message><type>ok</type></apiResponseMessage>';
	END

	-- ToDo Log Filename
