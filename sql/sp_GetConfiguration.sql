﻿

CREATE PROCEDURE [dbo].[sp_GetConfiguration]
@Key varchar(50),
@Variant varchar(50),
@ConfigValue varchar(150) output
AS

	SET NOCOUNT ON;

	SELECT @ConfigValue = [Value] FROM dbo.Configuration WHERE [Key] = @Key AND [Variant] = @Variant;

	
