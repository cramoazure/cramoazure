﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cramo_SearchAndRentAPI.Models
{
    public class TopicMessage
    {
        public string topic { get; set; }
        public int subscriberId { get; set; }
        public String messageBody { get; set; }
        public int channelId { get; set; }

        public String toString()
        {
            String response = "";
            response = response + "<topicMessageData>";
            response = response + "<topic>" + topic + "</topic>";
            response = response + "<subscriberId>" + subscriberId + "</subscriberId>";
            response = response + "<channelId>" + channelId + "</channelId>";
            response = response + "<body><![CDATA[" + messageBody + "]]></body>";
            response = response + "</topicMessageData>";

            return response;
            //return "<pimdata><topic>" + topic + "</topic><subscriberId>" + subscriberId + "</subscriberId><channelId>" + channelId); //+ "</channelId></pimdata>");
        }
    }
}