﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Cramo_SearchAndRentAPI.resources.utils
{
    public class HTTPHelper
    {

        public String createHTTPPost(String xmlContent, String uri, String apiKey)
        {
            String responseBody = "";

            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uri);
            myHttpWebRequest.Method = "POST";
            myHttpWebRequest.Headers.Set("Ocp-Apim-Subscription-Key", apiKey);

            byte[] data = Encoding.UTF8.GetBytes(xmlContent);

            myHttpWebRequest.ContentType = "application/xml";
            myHttpWebRequest.ContentLength = data.Length;

            Stream requestStream = myHttpWebRequest.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();

            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            Stream responseStream = myHttpWebResponse.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.UTF8);
            responseBody = myStreamReader.ReadToEnd();

            // Clean up
            myStreamReader.Close();
            responseStream.Close();
            myHttpWebResponse.Close();

            return responseBody;
        }
    }
}