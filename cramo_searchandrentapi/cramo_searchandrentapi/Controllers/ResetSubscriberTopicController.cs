﻿using Cramo_SearchAndRentAPI.Filters;
using Cramo_SearchAndRentAPI.Models;
using Cramo_SearchAndRentAPI.resources.utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cramo_SearchAndRentAPI.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class ResetSubscriberTopicController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get(int subscriberId, string topic)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            SubscriberTopic obj = new SubscriberTopic();

            obj.subscriberId = subscriberId;
            obj.topicName = topic;

            DBHelper dbObj = new DBHelper();
            ArrayList results = dbObj.resetSubscriberTopic(obj);

            String status = results[0].ToString();
            if (status.Equals("200"))
            {
                // Everything ok
                response.Content = new StringContent(results[1].ToString());
                response.StatusCode = HttpStatusCode.OK;

            }
            else if (status.Equals("400"))
            {
                response.Content = new StringContent(results[1].ToString());
                response.StatusCode = HttpStatusCode.BadRequest;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return response;

        }

    }
}
