﻿ALTER PROCEDURE [dbo].[sp_GetSRAConfiguration]
	@ProductId varchar(50),
	@Url varchar(255) output
AS

	SET NOCOUNT ON;

	DECLARE @localKey varchar(50);
	DECLARE @localVariant varchar(50);
	DECLARE @localResult varchar(150);

	SET @localKey = 'Documents';
	SET @localVariant = 'SRAURI';

	EXEC [dbo].[sp_GetConfiguration]
		@Key = @localKey,
		@Variant = @localVariant,
		@ConfigValue = @localResult OUTPUT

	SET @Url = @localResult + @ProductId;


	
