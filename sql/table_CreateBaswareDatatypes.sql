/*
CREATE TABLE [dbo].[BaswareDatatypes]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [Datatype] NVARCHAR(50) NOT NULL, 
    [FileName] NVARCHAR(255) NULL
)

*/ 

INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'suppliers', N'Suppliers$Market$_$Timestamp$_$RequestId$.xml');
INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'exchangerates', N'ExchangeRates$Market$_$Timestamp$_$RequestId$.xml');
INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'paymentterms', N'PaymentTerms$Market$_$Timestamp$_$RequestId$.xml');
INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'accounts', N'Accounts$Market$_$Timestamp$_$RequestId$.xml');
--INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'costcenters', N'CostCenters.xml');
--INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'costitems', N'CostItems.xml');
--INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'projects', N'Projects.xml');
--INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'phases', N'Phases.xml');
--INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'businesspartners', N'BusinessPartners.xml');
INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'transferresponse', N'TransferResponse$Market$_$Timestamp$_$RequestId$.xml');
INSERT INTO [dbo].[BaswareDatatypes] ( [Datatype], [Filename]) VALUES ( N'paymentresponse', N'PaymentResponse$Market$_$Timestamp$_$RequestId$.xml');

select * from [dbo].[BaswareDatatypes];
-- delete from  [dbo].[BaswareDatatypes];