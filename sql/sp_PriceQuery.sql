﻿USE [cramoesb]
GO

/****** Object: SqlProcedure [dbo].[sp_PostNewItem] Script Date: 2015-11-23 14:18:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_PriceQuery]
	@MessageBody nvarchar(max),
	@ResponseBody nvarchar(max) OUTPUT 
AS


	SET NOCOUNT ON;

	DECLARE @now datetime2;
	DECLARE @currentTimeStamp nvarchar(25);

	-- Get current timestamp
	SELECT @now = switchoffset (CONVERT(datetimeoffset, GETDATE()), '+01:00'); 
	SELECT @currentTimeStamp = convert(varchar,@now, 126);
	SET @currentTimeStamp = SUBSTRING(@currentTimeStamp, 1, 19) + '+0100';
	PRINT @currentTimeStamp;

	-- Get translations for market ->> Database/Company
	-- Get translations
	DECLARE @tmpKey        NVARCHAR(80);
    DECLARE @tmpFromValue  NVARCHAR(150);
    DECLARE @tmpToValue    NVARCHAR(150);
	DECLARE @tmpTranslations nvarchar(max);
	DECLARE @getid CURSOR

	SET @tmpTranslations = '';
	SET @getid = CURSOR FOR
		SELECT [Key], [FromValue], [ToValue] 
		FROM [dbo].[Translations] 
		WHERE [Key] like 'eCom.%.RentalDB';

	OPEN @getid
	FETCH NEXT FROM @getid INTO @tmpKey, @tmpFromValue, @tmpToValue

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @tmpTranslations = @tmpTranslations + '<Translation key="' + @tmpKey +  '"><from>' + @tmpFromValue +  '</from><to>' + @tmpToValue + '</to></Translation>';
		FETCH NEXT FROM @getid INTO @tmpKey, @tmpFromValue, @tmpToValue
	END
	-- PRINT @tmpTranslations

	SET @MessageBody = REPLACE(@MessageBody,'</order>', @xmlProperty + @tmpTranslations + '</order>');
	SET @ResponseBody = @MessageBody;

