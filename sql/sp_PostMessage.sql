USE [cramoesb]
GO
/****** Object:  StoredProcedure [dbo].[sp_PostMessage]    Script Date: 2015-12-22 08:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[sp_PostMessage]
@MessageBody nvarchar(max),
@Topic varchar(30),
@SubscriberId int = 0,
@Status int output,
@ResponseBody XML OUTPUT

AS

	SET NOCOUNT ON;

	DECLARE @topicID INT;
	DECLARE @messageID INT;
	declare @IdentityOutput table ( ID int )

	-- Validate incoming topic name
	SET @topicID = (SELECT topicID FROM dbo.Topic WHERE Name = @Topic);
	IF @TopicID IS NULL BEGIN
		-- ERROR, NOT A VALID TOPIC
		SET @ResponseBody = '<apiResponseMessage><message>Can''t process message, you sent a unknown topic (' + @Topic + ')</message><type>error</type></apiResponseMessage>'
		SET @Status = 400;
		RETURN;
	END

	-- Validate subscriberId. 0 is OK/default and means that message is sent to all registered subscribers. Otherwise subscriber must exist in dbo.SubscriberTopic for specified topic.
	IF @SubscriberId != 0 BEGIN

		IF NOT EXISTS(SELECT 1 FROM dbo.Subscriber WHERE SubscriberID = @SubscriberId)
		BEGIN
			SET @ResponseBody = '<apiResponseMessage><message>Can''t process message, unknown subscriber (' + CONVERT(VARCHAR(10), @SubscriberId) + ').</message><type>error</type></apiResponseMessage>'
			SET @Status = 400;
			RETURN;
		END
		ELSE 
			IF NOT EXISTS(SELECT 1 FROM dbo.SubscriberTopic WHERE SubscriberID = @SubscriberId AND TopicId = @TopicId)
			BEGIN
				SET @ResponseBody = '<apiResponseMessage><message>Can''t process message, there is no registered subscription for subscriber ' + CONVERT(VARCHAR(10), @SubscriberId) + ' on topic ' + @topic + '.</message><type>error</type></apiResponseMessage>'
				SET @Status = 400;
				RETURN;
			END
		END
		
		BEGIN TRANSACTION

			INSERT dbo.Message (TopicID, BodyText, Received) 
			OUTPUT INSERTED.MessageID INTO @IdentityOutput
			--VALUES (@topicID, CONVERT(XML, @messagebody COLLATE SQL_Latin1_General_CP1_CI_AS), getdate());
			VALUES (@topicID, @messagebody, getdate());

			SET @messageID = (SELECT ID FROM @IdentityOutput);

			-- för varje subscriber i dbo.SubscriberTopic, lägg till en post i SubscriberMessageQueue
			IF @SubscriberId = 0 BEGIN
				INSERT dbo.SubscriberMessageQueue (MessageID, SubscriberID)
				SELECT @messageID AS MessageID, st.SubscriberID
				FROM dbo.SubscriberTopic st
				WHERE st.TopicID = @topicID;
			END
			ELSE
			BEGIN

				INSERT dbo.SubscriberMessageQueue (MessageID, SubscriberID)
				VALUES (@messageID, @SubscriberId)


			END
		COMMIT TRANSACTION

		SET @Status = 200;
		SET @ResponseBody = '<apiResponseMessage><message>Document received as messageID ' + CAST(@messageID AS varchar(10)) + ' in topic ' + @Topic + '.</message><type>ok</type></apiResponseMessage>'


