﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Cramo_SearchAndRentAPI.Models;
using Cramo_SearchAndRentAPI.resources.utils;
using System.Collections;
using Cramo_SearchAndRentAPI.Filters;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Xml;
using Newtonsoft.Json;

namespace Cramo_SearchAndRentAPI.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class TopicMessageController : ApiController
    {
        // GET
        [HttpGet]
        public HttpResponseMessage Get(String topic, String subscriberId, String batchSize = "1")
        {

            HttpResponseMessage response = new HttpResponseMessage();

            // Init database
            DBHelper dbObj = new DBHelper();
            dbObj.init();

            try
            {

                String results = dbObj.getTopicMessages(topic, subscriberId, batchSize);

                if (Request.Headers.Accept.ToString() == "application/json")
                {

                    var xml = XElement.Parse(results);
                    string jsonResult = JsonConvert.SerializeXNode(xml);
                    response.Content = new StringContent(jsonResult);

                }
                else
                {

                    response.Content = new StringContent(results);
                
                }

                response.StatusCode = HttpStatusCode.OK;

            }
            catch (Exception e)
            {
                response.Content = new StringContent(e.ToString());
                response.StatusCode = HttpStatusCode.InternalServerError;

            }

            // ClearUp
            dbObj.dispose();
            dbObj = null;

            return response;
        }

        // POST 
        [HttpPost]
        public HttpResponseMessage Post([FromBody]String messageBody, [FromUri]String topic, [FromUri]int channelId = 0, [FromUri]int subscriberId = 0)
        {

            if (topic == "orders" && !(OrderValidator.IsOrderValid(messageBody)))
                return new HttpResponseMessage(HttpStatusCode.OK);

            // Try to get the body
            // String content = Request.Content.ReadAsStringAsync();

            HttpResponseMessage response = new HttpResponseMessage();
            TopicMessage obj = new TopicMessage();
            obj.channelId = channelId;
            obj.subscriberId = subscriberId;
            obj.topic = topic;
            obj.messageBody = messageBody;

            //response.Content = new StringContent(obj.toString());
            //response.StatusCode = HttpStatusCode.OK;
            //return response;


            if (messageBody == null || messageBody.Trim().Equals(""))
            {
                // Return immediately
                response.Content = new StringContent("<apiResponseMessage><message>You need to pass a message to store on queue!</message><type>error</type></apiResponseMessage>");
                response.StatusCode = HttpStatusCode.BadRequest;

            }

            // Call DB to insert data
            DBHelper dbObj = new DBHelper();
            ArrayList results = dbObj.storeTopicMessage(obj);

            // Check response

            String status = results[0].ToString();
            if (status.Equals("200"))
            {
                // Everything ok
                response.Content = new StringContent(results[1].ToString());
                response.StatusCode = HttpStatusCode.OK;

            }
            else if (status.Equals("400"))
            {
                response.Content = new StringContent(results[1].ToString());
                response.StatusCode = HttpStatusCode.BadRequest;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return response;

        }
    }
}
