﻿Use [master];
GO

CREATE LOGIN integrationusrtst 
		WITH PASSWORD = 'AsW5ThcEyjP9ksf';

CREATE LOGIN integrationusrprd 
		WITH PASSWORD = '9xwtWbvr3PGIWLq';


-- ==== PROD =====
Use [cramoesb];
GO

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'integrationusrprd')
BEGIN
	
    CREATE USER integrationusrprd FOR LOGIN integrationusrprd;
    EXEC sp_addrolemember N'db_owner', N'integrationusrprd';
END;
GO

-- ==== TEST =====
Use [cramoesb-Test];
GO

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'integrationusrtst')
BEGIN
	
    CREATE USER [integrationusrtst] FOR LOGIN [integrationusrtst];
    EXEC sp_addrolemember N'db_owner', N'integrationusrtst';
END;
GO