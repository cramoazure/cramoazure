﻿USE [cramoesb]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_InsertLogRecord]
	@LogApplication varchar(80) = '',
    @LogKey varchar(80),
    @LogStep varchar(25) = '',
	@LogText varchar(500) = '',
	@LogMessage varchar(max) = ''
AS
    SET NOCOUNT ON;
    
	IF @LogMessage IS NULL BEGIN 
		SET @LogMessage = '';
	END

	-- INSERT LOG ROW
	BEGIN TRY
		INSERT INTO [Log] ([Application], [Key], [LogStep], [LogText], [Message]) VALUES (@LogApplication, @LogKey, @LogStep, @LogText, @LogMessage);
	END TRY
	BEGIN CATCH 
		-- Possible future handling, Status=400?
	END CATCH
